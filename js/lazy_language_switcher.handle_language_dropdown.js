/**
 * @file
 * Lazy language switcher behaviors.
 */
(function (Drupal, once) {

  'use strict';

  Drupal.behaviors.lazyLanguageSwitcher = {
    attach: function (context, settings) {
      let currentLanguage = ''; 
      
      once('lazyLanguageSwitcher', '._msddli_', context).forEach(function (element) {
        element.addEventListener('mouseover', function(event) {
          currentLanguage = document.querySelector('.ddTitleText .ddlabel').innerHTML;
        });
        
        element.addEventListener('click', function(event){
          document.querySelector('.ddTitleText .ddlabel').innerHTML = currentLanguage;
        });
      });
    }
  };

})(Drupal, once);
